<p>Pour que le plugin fonctionne, il faut supprimer ou mettre en commentaire, la ligne de vérification du captcha par défaut dans votre thème.</p>
<p>Dans le thème par défaut, cela concerne la ligne +/- 80 du fichier commentaires.php, située après <code>&lt;?php $plxShow->capchaQ(); ?&gt;</code>
  <br/>
<code>&lt;input id="id_rep" name="rep" type="text" size="2" maxlength="1" style="width: auto; display: inline;" /&gt;</code>
<p>Le plugin est livré avec une série de questions/réponses qu'il est conseillé de ne pas conserver afin que les fichiers ne soient pas indexés depuis les dépôts git.</p>
<p>Créer vos propres couples de questions/réponses permettra de rendre votre site unique et plus difficile à spammer.</p>
<p>Il y a également des questions sans réponses qui permettent de masquer les vraies questions. Plus il y en aura, plus mince sera la possibilité de spammer.</p>